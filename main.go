/*
Copyright © 2022 MARC WINKLER marc.winkler@id.ethz.ch
*/
package main

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.ethz.ch/itlehrlabor/netcenter-client-go/api"
	// "gitlab.ethz.ch/itlehrlabor/netcenter-client-go/cmd"
)

func init() {
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

}

func main() {
	// cmd.Execute()
	// Loads username and password from Environment Variables.
	var username string = os.Getenv("NETCENTER_USERNAME")
	var password string = os.Getenv("NETCENTER_PASSWORD")
	if username == "" || password == "" {
		fmt.Println("Username and Password missing. Exiting.")
		return
	}
	client, err := api.NewClient(nil, &username, &password)
	if err != nil {
		fmt.Printf("Error occured: %s", err)
		return
	}

	// req, err := http.NewRequest("GET", fmt.Sprintf("%s/nameToIP/fqName/itl-core-jmp01.itlehrlabor.ethz.ch", client.NetcenterApiURL), strings.NewReader(""))
	// if err != nil {
	// 	fmt.Printf("Error occurred: %s", err)
	// 	return
	// }
	// body, err := client.DoRequest(req)
	// if err != nil {
	// 	fmt.Printf("Error ocurred: %s", err)
	// 	return
	// }
	// fmt.Printf("what is this: %s", body)

	//data := "<usedIps><usedIp><ip>129.132.178.226</ip><ipSubnet>129.132.178.224</ipSubnet><fqname>itl-core-jmp01.itlehrlabor.ethz.ch</fqname><forward>Y</forward><reverse>Y</reverse><ttl>3600</ttl><dhcp>Y</dhcp><dhcpMac>00-50-56-A5-24-8E</dhcpMac><ddns>N</ddns><isgGroup>it-lehrlabor</isgGroup><lastDetection>2022-03-01 09:21</lastDetection><remark>Jump Server residing on ESXi environment of ID</remark><alias>N</alias><lastChange>2018-06-04 15:02</lastChange><lastChangeBy>winklmar</lastChangeBy><ipVersion>v4</ipVersion><views><view>extern</view><view>intern</view></views></usedIp></usedIps>"
	fqData, err := client.GetNameToIpRecord("itl-monitoringsrv01.ethz.ch")
	if err != nil {
		fmt.Println("error occurred: " + err.Error())
		return
	}
	// bd := []byte(data)
	// var netIps api.IpRecords
	// xml.Unmarshal(bd, &netIps)
	// xmlPacket := &api.IpRecords{}

	// _ = xml.Unmarshal(bd, &xmlPacket)
	fmt.Println("FQ Hostname Lookup")
	for i := 0; i < len(fqData.IPs); i++ {
		fmt.Println(fqData.IPs[i].Views.Views)
		fmt.Println("IP Address: " + fqData.IPs[i].IP)
		fmt.Println("ISG: " + fqData.IPs[i].IsgGroup)
		fmt.Println("FQDN: " + fqData.IPs[i].FqName)
		fmt.Println("IP Version: " + fqData.IPs[i].IpVersion)
	}

	ipData, err := client.GetNameToIpIpRecord("129.132.79.229")
	if err != nil {
		fmt.Println("error occurred: " + err.Error())
	}
	// bd := []byte(data)
	// var netIps api.IpRecords
	// xml.Unmarshal(bd, &netIps)
	// xmlPacket := &api.IpRecords{}
	fmt.Println("------------")
	fmt.Println("IPv4 Lookup")
	// _ = xml.Unmarshal(bd, &xmlPacket)
	for i := 0; i < len(ipData.IPs); i++ {
		fmt.Println(ipData.IPs[i].Views.Views)
		fmt.Println("IP Address: " + ipData.IPs[i].IP)
		fmt.Println("ISG: " + ipData.IPs[i].IsgGroup)
		fmt.Println("FQDN: " + ipData.IPs[i].FqName)
		fmt.Println("IP Version: " + ipData.IPs[i].IpVersion)
	}

	ipv6Data, err := client.GetNameToIpIpRecord("2001:67c:10ec:4941::101")
	if err != nil {
		fmt.Println("error occurred: " + err.Error())
	}
	// bd := []byte(data)
	// var netIps api.IpRecords
	// xml.Unmarshal(bd, &netIps)
	// xmlPacket := &api.IpRecords{}
	fmt.Println("------------")
	fmt.Println("IPv6 Lookup")
	// _ = xml.Unmarshal(bd, &xmlPacket)
	for i := 0; i < len(ipv6Data.IPs); i++ {
		fmt.Println(ipv6Data.IPs[i].Views.Views)
		fmt.Println("IP Address: " + ipv6Data.IPs[i].IP)
		fmt.Println("ISG: " + ipv6Data.IPs[i].IsgGroup)
		fmt.Println("FQDN: " + ipv6Data.IPs[i].FqName)
		fmt.Println("IP Version: " + ipv6Data.IPs[i].IpVersion)
	}

	var newAlias api.AliasInsertRecord
	newAlias.FqName = "test-winklmar.ethz.ch"
	newAlias.HostName = "hallotest.ethz.ch"
	newAlias.Ttl = 3600
	newAlias.Views = append(newAlias.Views, "extern")
	newAlias.Views = append(newAlias.Views, "intern")

	test, err := client.CreateAliasRecord(newAlias)
	if err != nil {
		fmt.Printf("Something went wrong: %v\n", err)
	} else {
		fmt.Printf("Alias successfully created. %v", test.FqName)
	}

	errors := client.DeleteAliasRecord("test-winklmar.ethz.ch")
	if errors != nil {
		fmt.Printf("Something went wrong: %v\n", errors)
	}

	var newIpThing api.IPv4InsertRecord
	newIpThing.IP = "129.132.79.229"
	newIpThing.FqName = "hallo2000.ethz.ch"
	newIpThing.IsgGroup = "it-lehrlabor"
	newIpThing.Views = []string{"intern", "extern"}
	newIpThing.Forward = "Y"
	newIpThing.Reverse = "N"
	fmt.Printf("newThingIp is: %v", newIpThing)
	thing, err := client.CreateNameToIPv4Record(newIpThing)
	if err != nil {
		fmt.Printf("IPv4 Record Creation failed. Netcenter said: %v\n", err)
	}
	fmt.Printf("Thing is: %v", thing)

	var newIpv6Thing api.IPv6InsertRecord
	newIpv6Thing.IP = "2001:67c:10ec:4944::21"
	newIpv6Thing.FqName = "hallo2000.ethz.ch"
	newIpv6Thing.IsgGroup = "it-lehrlabor"
	newIpv6Thing.Views = []string{"intern", "extern"}
	newIpv6Thing.Forward = "Y"
	newIpv6Thing.Reverse = "N"
	fmt.Printf("newThingIp is: %v", newIpv6Thing)
	thing6, err := client.CreateNameToIPv6Record(newIpv6Thing)
	if err != nil {
		fmt.Printf("IPv6 Record Creation failed. Netcenter said: %v\n", err)
	}
	fmt.Printf("Thing is: %v", thing6)

	acmeThing, err := client.EnableLetsEncrypt("hallo2000.ethz.ch", "nameToIp")
	if err != nil {
		fmt.Printf("Let's Encrypt for Host: hallo2000.ethz.ch not enabled. Error: %v\n", err)
	}
	fmt.Printf("AcmeThing is: %v", acmeThing)

	stat, err := client.DisableLetsEncrypt("hallo2000.ethz.ch", "nameToIp")
	if err != nil {
		fmt.Printf("Unable to Disable Let's Encrypt for Host hallo2000.ethz.ch, error: %v\n", err)
	}
	fmt.Printf("Let's Encrypt for host hallo2000.ethz.ch successfully deleted. Status of Request: %v", stat)

	client.DeleteNameToIPRecord("129.132.79.229", "hallo2000.ethz.ch")
	client.DeleteNameToIPRecord("2001:67c:10ec:4944::21", "hallo2000.ethz.ch")
}
