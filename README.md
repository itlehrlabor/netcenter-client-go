# Netcenter GO API

## WHY?

This is an Netcenter API Client written in Go an its primary use is for the Netcenter Terraform Provider.

## Implemented Endpoints

Because the Netcenter API provides a vast array of endpoints and every one of them has a special request and response structure, not all of the endpoints are implemented.

The following table contains information of which endpoints are implemented and what function is available.

| Endpoint         | Implemented   | implemented functions                          | Documentation Link |
|------------------|:-------------:|------------------------------------------------|--------------------|
| Alias            |      ✅       | Create (PUT)<br/>Get (GET)<br/>Delete (DELETE) | https://unlimited.ethz.ch/pages/viewpage.action?spaceKey=NCP&title=WebServices+IP
|  NameToIp        |      ✅       | Create (PUT)<br/>Get (GET)<br/>Delete (DELETE)| https://unlimited.ethz.ch/pages/viewpage.action?spaceKey=NCP&title=WebServices+IP
|  Firewall        |      ⬜️       |                                               |
|  Manage Let's Encryot        |      ✅       | The API can enable and disable Let's Encrypt on NameToIp Entries. | https://unlimited.ethz.ch/pages/viewpage.action?pageId=29249399

## Usage

This API should be used in a Go Project, but if you want to test it, you can run the main.go file and provide the following configuration through env-variables (but this is only for testing).

* 


## Last Update

28. Juli 2022