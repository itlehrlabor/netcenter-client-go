/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var dnsRecordType string

// getDnsRecordCmd represents the getDnsRecord command
var getDnsRecordCmd = &cobra.Command{
	Use:   "getDnsRecord",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("getDnsRecord called")
		fmt.Println(fmt.Sprintf("Searching for %s record type(s) with username %s and password %s", dnsRecordType, username, password))
	},
}

func init() {
	rootCmd.AddCommand(getDnsRecordCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	getDnsRecordCmd.PersistentFlags().StringVar(&dnsRecordType, "type", "all", "What record type you want to search. Default: All record types are searched.")

	getDnsRecordCmd.PersistentFlags().String("hostname", "", "What hostname to look for / create.")
	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getDnsRecordCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
