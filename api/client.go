package api

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// This file describes functions to interact with the Netcenter HTTP API
// 'NewClient' instaniates a new Client Interface, that can be used to make API calls
// and the doRequest Function enables you to actually make the request. It always returns the
// Response body and a error message (if there is one).

// NetcenterApiURL - Default Production URL for Netcenter
const NetcenterApiURL string = "https://www.netcenter.ethz.ch/netcenter/rest"

// Client construct for standard data
type Client struct {
	NetcenterApiURL string
	Username        string
	Password        string
	HTTPClient      *http.Client
}

// Function to create NewClient
func NewClient(host, username, password *string) (client *Client, err error) {
	client = &Client{
		HTTPClient: &http.Client{Timeout: 10 * time.Second},
		// Set HostURL to Netcenter default URL if host is not provided
		NetcenterApiURL: NetcenterApiURL}

	if host != nil {
		client.NetcenterApiURL = *host
	}

	// If Username is not provided, return empty client
	if username == nil || password == nil {
		return client, nil
	}

	client.Username = *username
	client.Password = *password

	return client, err
}

// Sends a request
func (c *Client) DoRequest(req *http.Request) ([]byte, error) {
	// Send request and capture response and error
	req.SetBasicAuth(c.Username, c.Password)
	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("http status code: %d, body: %s", res.StatusCode, body)
	}

	// Parsing the Status of the request from the Netcenter Response
	// Extrapolating XML Data into a suitable struct
	// This is a stupid implementation, but the only way I see how.
	// Check if the response contains the string "success" or "errors" -
	// Becaues the Netcenter API does not set the corret HTTP Status Code.
	if strings.Contains(string(body[:]), "success") {
		resStatus := ResponseModelSucess{}
		err = xml.Unmarshal(body, &resStatus)
		if err != nil {
			return body, err
		}
	} else if strings.Contains(string(body[:]), "errors") {
		resStatus := ResponseModelError{}
		err = xml.Unmarshal(body, &resStatus)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("error accessing Netcenter with the following error: %v", resStatus.ErrorMsg)
	}

	return body, err
}
