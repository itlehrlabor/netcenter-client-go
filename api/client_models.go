package api

import "encoding/xml"

type ResponseModelSucess struct {
	XMLName xml.Name `xml:"success"`
	Success string   `xml:"success"`
}

type ResponseModelError struct {
	XMLName  xml.Name `xml:"errors"`
	ErrorMsg string   `xml:"error>msg"`
}
