package api

// This file contains all functions to consume $netcenter_url/netcenter/rest/nameToIP endpoint has to offer.
// This includes:
// - IP-Nummer erstellen (POST)
// - IP-Nummer löschen (DELETE)
// - IP-Nummer mutieren (PUT)
// - IP-Nummer sichten (GET)
// Documentation at: https://unlimited.ethz.ch/display/NCP/WebServices+IP

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
)

const nameToIPEndpoint string = "nameToIP"
const AcmeDnsBaseUrl string = "https://acme.ethz.ch"

// Get IP Entries based on hostname (FQDN). This takes in a hostname in fqdn format and outputs a IpRecords struct
func (c *Client) GetNameToIpRecord(hostname string) (*IpRecords, error) {
	// IP-Nummer sichten (GET) - Gets a IP Record and returns a IPRecords Element

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/%s/fqName/%s", c.NetcenterApiURL, nameToIPEndpoint, hostname), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		log.WithFields(log.Fields{
			"Record": err,
		}).Debug("Error ocurred during request.")
		return nil, err
	}

	records := IpRecords{}
	err = xml.Unmarshal(body, &records)
	if err != nil {
		log.WithFields(log.Fields{
			"Record": err,
		}).Debug("Error ocurred during xml unmarshal.")
		return nil, err
	}

	log.WithFields(log.Fields{
		"Record": records,
	}).Debug("Got the following IP record from Netcenter.")

	return &records, err
}

// Get IP Entries based on IP Address. This takes in a IPv4 or IPv6 address and outputs a IpRecords struct
func (c *Client) GetNameToIpIpRecord(ipaddress string) (*IpRecords, error) {
	// IP Nummer sichten (GET) - Gets IP Records based on IP Address and returns a IPRecords Element

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/%s/%s", c.NetcenterApiURL, nameToIPEndpoint, ipaddress), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		log.WithFields(log.Fields{
			"Record": err,
		}).Debug("Error ocurred during request.")
		return nil, err
	}

	records := IpRecords{}
	err = xml.Unmarshal(body, &records)
	if err != nil {
		log.WithFields(log.Fields{
			"Record": err,
		}).Debug("Error ocurred during request.")
		return nil, err
	}
	// If you lookup based on IP Address, netcenter does not return the IP-Version (which makes sense).
	// To clarify the return data, this block adds the ip-version to the output element.
	for i := 0; i < len(records.IPs); i++ {
		if strings.Contains(records.IPs[i].IP, ".") {
			records.IPs[i].IpVersion = "v4"
		} else {
			records.IPs[i].IpVersion = "v6"
		}
	}
	log.WithFields(log.Fields{
		"Record": records,
	}).Debug("Got the following IP record from Netcenter.")

	return &records, err
}

// Create IPv4 Entry. This takes in a IPv4InsertRecord and outputs a corresponding Record
func (c *Client) CreateNameToIPv4Record(rec IPv4InsertRecord) (*IPv4InsertRecord, error) {
	reqbody, err := xml.Marshal(rec)
	if err != nil {
		return nil, err
	}
	log.WithFields(log.Fields{
		"IPv4Record": string(reqbody),
	}).Debug("Creating IPv4 Record.")

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/%s", c.NetcenterApiURL, nameToIPEndpoint), strings.NewReader(string(reqbody)))
	if err != nil {
		return nil, err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		return nil, err
	}

	log.WithField("Request Status", string(body)).Debug("IPv4 Record successfully created.")

	return &rec, nil
}

// Create IPv4 Entry. This takes in a IPv4InsertRecord and outputs a corresponding Record
func (c *Client) CreateNameToIPv6Record(rec IPv6InsertRecord) (*IPv6InsertRecord, error) {
	reqbody, err := xml.Marshal(rec)
	if err != nil {
		return nil, err
	}
	log.WithFields(log.Fields{
		"IPv4Record": string(reqbody),
	}).Debug("Creating IPv6 Record.")

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/%s", c.NetcenterApiURL, nameToIPEndpoint), strings.NewReader(string(reqbody)))
	if err != nil {
		return nil, err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		return nil, err
	}

	log.WithField("Request Status", string(body)).Debug("IPv6 Record successfully created.")

	return &rec, nil
}

// Enable Let's Encrypt on NameToIp or Alias Record. This Takes in a hostname in fqdn format
// and a recordType "alias" or "nameToIp" and returns a Type with all info about AcmeDNS Let's Encrypt
func (c *Client) EnableLetsEncrypt(hostname string, recordType string) (*AcmeData, error) {
	log.WithFields(log.Fields{
		"Hostname":   string(hostname),
		"RecordType": string(recordType),
	}).Debug("Enabling Let's Encrypt on Hostname %s of Type: %s", hostname, recordType)

	// Set ApiEndpoint based on Record Type
	ApiEndpoint := ""
	if recordType == "alias" {
		ApiEndpoint = "alias"
	} else if recordType == "nameToIp" {
		ApiEndpoint = "nameToIP/fqName"
	}

	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/%s/%s/enableAcmeDns", c.NetcenterApiURL, ApiEndpoint, hostname), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		return nil, err
	}

	records := IpRecords{}
	err = xml.Unmarshal(body, &records)
	if err != nil {
		log.WithFields(log.Fields{
			"Error": err,
		}).Debug("Error ocurred during request.")
		return nil, err
	}

	output := AcmeData{}
	output.AcmeBaseUrl = AcmeDnsBaseUrl
	output.AcmeUsername = records.IPs[0].AcmeUsername
	output.AcmeSubdomain = records.IPs[0].AcmeSubdomain
	output.AcmePassword = records.IPs[0].AcmePassword

	log.WithField("Request Status", string(body)).Debug("Let's encrypt successfully created.")

	return &output, nil
}

// Disable Let's Encrypt on NameToIp or Alias Record. This Takes in a hostname in fqdn format
// and a recordType "alias" or "nameToIp" and returns true or false if Let's Encrypt was successfully deleted.
func (c *Client) DisableLetsEncrypt(hostname string, recordType string) (bool, error) {
	log.WithFields(log.Fields{
		"Hostname":   string(hostname),
		"RecordType": string(recordType),
	}).Debug("Disabling Let's Encrypt on Hostname %s of Type: %s", hostname, recordType)

	// Set ApiEndpoint based on Record Type
	ApiEndpoint := ""
	if recordType == "alias" {
		ApiEndpoint = "alias"
	} else if recordType == "nameToIp" {
		ApiEndpoint = "nameToIP/fqName"
	}

	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/%s/%s/disableAcmeDns", c.NetcenterApiURL, ApiEndpoint, hostname), nil)
	if err != nil {
		return false, err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		log.WithField("Request Status", string(body)).Debug("Error while disabling Let's encrypt.")
		return false, err
	}
	log.WithField("Request Status", string(body)).Debug("Let's encrypt successfully deleted.")
	return true, nil
}

// Create IPv4 Entry. This takes in a IPv4InsertRecord and outputs a corresponding Record
func (c *Client) DeleteNameToIPRecord(ipAddress, hostname string) error {
	log.WithFields(log.Fields{
		"ToBeDeletedIP":       ipAddress,
		"ToBeDeletedHostname": hostname,
	}).Debug("Deleting IP Record with the following IP and Hostname.")

	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/%s/%s/%s", c.NetcenterApiURL, nameToIPEndpoint, ipAddress, hostname), nil)
	if err != nil {
		return err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		return err
	}

	log.WithField("Request Status", string(body)).Debug("IP and Hostname successfully deleted.")

	return nil
}
