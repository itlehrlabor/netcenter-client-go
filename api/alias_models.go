package api

import "encoding/xml"

// This file contains all the alias schemas necessary to convert Netcenter
// XML response data into a separate structure.

// The XML Structure for Aliases endpoint
// URL for ip Lookup: $netcenter_url/netcenter/rest/alias/
// URL for Hostname Lookup: $netcenter_url/netcenter/rest/alias/fqName/[FQDN Hostname]

// Structure for Aliases
type AliasRecords struct {
	XMLName xml.Name      `xml:"usedAliases"`
	Aliases []AliasRecord `xml:"alias"`
}

type AliasRecord struct {
	XMLName       xml.Name  `xml:"alias"`
	FqName        string    `xml:"fqName"`
	HostName      string    `xml:"hostName"`
	Ttl           int       `xml:"ttl"`
	Remark        string    `xml:"remark"`
	NetsupGroup   string    `xml:"netsupGroup"`
	Views         AliasView `xml:"views"`
	AcmeUsername  string    `xml:"acmeUsername,omitempty"`
	AcmePassword  string    `xml:"acmePassword,omitempty"`
	AcmeSubdomain string    `xml:"acmeSubdomain,omitempty"`
	recordType    string
}

type AliasView struct {
	XMLName xml.Name `xml:"views"`
	Views   []string `xml:"view"`
}

type AliasInsertRecord struct {
	XMLName     xml.Name `xml:"insert"`
	FqName      string   `xml:"alias>fqName"`
	HostName    string   `xml:"alias>hostName"`
	Ttl         int      `xml:"alias>ttl"`
	NetsupGroup string   `xml:"alias>netsupGroup"`
	Views       []string `xml:"alias>views>view"`
}
