package api

import (
	"encoding/xml"
)

// This file contains all the nametoip schemas necessary to convert Netcenter
// XML response data into a separate structure.

// The XML Structure for NameToIp and NameToIP fqname endpoint
// URL for ip Lookup: $netcenter_url/netcenter/rest/NameToIP/[IP_ADDRESS]
// URL for Hostname Lookup: $netcenter_url/netcenter/rest/NameToIP/fqName/[FQDN Hostname]
type IpRecords struct {
	XMLName xml.Name   `xml:"usedIps"`
	IPs     []IpRecord `xml:"usedIp"`
}

type IpRecord struct {
	XMLName       xml.Name     `xml:"usedIp"`
	FqName        string       `xml:"fqname"`
	IP            string       `xml:"ip"`
	Forward       string       `xml:"forward"`
	Reverse       string       `xml:"reverse"`
	TTL           int          `xml:"ttl"`
	IpVersion     string       `xml:"ipVersion"`
	IsgGroup      string       `xml:"isgGroup"`
	DhcpEnabled   string       `xml:"dhcp"`
	DhcpMac       string       `xml:"dhcpMac"`
	LastDetection string       `xml:"lastDetecion"`
	Views         IpRecordView `xml:"views"`
	AcmeUsername  string       `xml:"acmeUsername,omitempty"`
	AcmePassword  string       `xml:"acmePassword,omitempty"`
	AcmeSubdomain string       `xml:"acmeSubdomain,omitempty"`
}
type IpRecordView struct {
	XMLName xml.Name `xml:"views"`
	Views   []string `xml:"view"`
}

type IPv4InsertRecord struct {
	XMLName  xml.Name `xml:"insert"`
	IP       string   `xml:"nameToIP>ip"`
	FqName   string   `xml:"nameToIP>fqName"`
	Forward  string   `xml:"nameToIP>forward,omitempty"`
	Reverse  string   `xml:"nameToIP>reverse,omitempty"`
	Ttl      int      `xml:"nameToIP>ttl,omitempty"`
	Dhcp     string   `xml:"nameToIP>dhcp,omitempty"`
	DhcpMac  string   `xml:"nameToIP>dhcpMac,omitempty"`
	Ddns     string   `xml:"nameToIP>ddns,omitempty"`
	IsgGroup string   `xml:"nameToIP>isgGroup"`
	Views    []string `xml:"nameToIP>views>view,omitempty"`
}

type IPv6InsertRecord struct {
	XMLName  xml.Name `xml:"insert"`
	IP       string   `xml:"nameToIP>ipv6"`
	FqName   string   `xml:"nameToIP>fqName"`
	Forward  string   `xml:"nameToIP>forward,omitempty"`
	Reverse  string   `xml:"nameToIP>reverse,omitempty"`
	Ttl      int      `xml:"nameToIP>ttl,omitempty"`
	Dhcp     string   `xml:"nameToIP>dhcp,omitempty"`
	DhcpMac  string   `xml:"nameToIP>dhcpMac,omitempty"`
	Ddns     string   `xml:"nameToIP>ddns,omitempty"`
	IsgGroup string   `xml:"nameToIP>isgGroup"`
	Views    []string `xml:"nameToIP>views>view,omitempty"`
}
