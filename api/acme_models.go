package api

// This file contains a simple model for storing Let's Encrypt Data
// returned from the Netcenter API.

type AcmeData struct {
	AcmeBaseUrl   string
	AcmeUsername  string
	AcmePassword  string
	AcmeSubdomain string
}
