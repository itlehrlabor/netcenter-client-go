package api

// This file contains all functions to consume $netcenter_url/netcenter/rest/alias endpoint has to offer.
// This includes:
// - Alias erstellen (POST)
// - Alias löschen (DELETE)
// - Alias mutieren (PUT)
// - Alias sichten (GET)
// Documentation at: https://unlimited.ethz.ch/display/NCP/WebServices+IP

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
)

const nameAliasEndpoint string = "alias"

// Get IP Entries based on hostname (FQDN). This takes in a hostname in fqdn format and outputs a IpRecords struct
func (c *Client) GetAliasRecord(hostname string) (*AliasRecords, error) {
	// Alias sichten (GET) - Gets a Alias Record and returns a AliasRecords Element

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/%s/%s", c.NetcenterApiURL, nameAliasEndpoint, hostname), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		return nil, err
	}

	records := AliasRecords{}
	err = xml.Unmarshal(body, &records)
	if err != nil {
		return nil, err
	}
	log.WithFields(log.Fields{
		"Record": records,
	}).Debug("Got the following Alias record from Netcenter.")
	return &records, err
}

// Create Alias Record based on Alias, requires a AliasInsertRecord struct. This outputs a AliasRecords struct
func (c *Client) CreateAliasRecord(newAliasItem AliasInsertRecord) (*AliasInsertRecord, error) {
	reqbody, err := xml.Marshal(newAliasItem)
	if err != nil {
		return nil, err
	}
	log.WithFields(log.Fields{
		"NewAlias": string(reqbody),
	}).Debug("Creating new Alias Record.")

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/%s", c.NetcenterApiURL, nameAliasEndpoint), strings.NewReader(string(reqbody)))
	if err != nil {
		return nil, err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		return nil, err
	}

	log.WithField("Request Status", string(body)).Debug("Alias successfully created.")

	return &newAliasItem, nil
}

// Delete Alias Record based on Alias, requires a Alias fqdn hostname. Does not return anything if successful
func (c *Client) DeleteAliasRecord(fqdnHostname string) error {
	log.WithFields(log.Fields{
		"ToBeDeletedAlias": fqdnHostname,
	}).Debug("Deleting Alias with the following Name")

	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/%s/%s", c.NetcenterApiURL, nameAliasEndpoint, fqdnHostname), nil)
	if err != nil {
		return err
	}

	body, err := c.DoRequest(req)
	if err != nil {
		return err
	}

	log.WithField("Request Status", string(body)).Debug("Alias successfully deleted.")

	return nil
}
